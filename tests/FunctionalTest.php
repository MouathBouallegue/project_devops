<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Client;
use App\Entity\Voiture;
use App\Entity\Location;

class FunctionalTest extends WebTestCase
{
    public function testClientCrudOperations()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler= $client->request('GET','/client');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1','Client index');
    }

    public function testVoitureCrudOperations()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler= $client->request('GET','/voiture');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1','Voiture index');
    }

    public function testLocationCrudOperations()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler= $client->request('GET','/location');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1','Location index');
    }

    public function testShouldDisplayCreateNewClient()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/client/new');
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Create new Client');
    }

    public function testShouldDisplayCreateNewVoiture()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/voiture/new');
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Create new Voiture');
    }
    
    public function testShouldDisplayCreateNewLocation()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/location/new');
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Create new Location');
    }

    public function testShouldAddNewClient()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/client/new');
    $buttonCrawlerNode = $crawler->selectButton('Save');
    $form = $buttonCrawlerNode->form();
    $uuid = uniqid();
    $form = $buttonCrawlerNode->form([
        'client[cin]' => 12345678, 
        'client[nom]' => 'John',    
        'client[prenom]' => 'Doe', 
        'client[adresse]' => 'Tunis'
    ]);
    $client->submit($form) ;
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'John Doe');
    }

    public function testShouldAddNewVoiture()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/voiture/new');
    $buttonCrawlerNode = $crawler->selectButton('Save');
    $form = $buttonCrawlerNode->form();
    $uuid = uniqid();
    $form = $buttonCrawlerNode->form([
        'voiture[serie]' => '123-TUN-4567', 
        'voiture[dateMiseEnMarche][year]' => '2023',
        'voiture[dateMiseEnMarche][month]' => '12',
        'voiture[dateMiseEnMarche][day]' => '29',     
        'voiture[modele]' => 'Kia', 
        'voiture[prixJour]' => 50.0
    ]);
    $client->submit($form) ;
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', '123-TUN-4567 2023-12-29 Kia 50');
    }

    public function testShouldAddNewLocation()
    {
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET','/location/new');
    $buttonCrawlerNode = $crawler->selectButton('Save');
    $form = $buttonCrawlerNode->form();
    $uuid = uniqid();
    $form = $buttonCrawlerNode->form([
        'location[date_debut][year]' => 2023,
        'location[date_debut][month]' => 12,
        'location[date_debut][day]' => 01,  
        'location[date_retour][year]' => 2023,
        'location[date_retour][month]' => 12,
        'location[date_retour][day]' => 11,    
        'location[prix]' =>  500.0,
        'location[voiture]'=>1,
        'location[client]'=>1
    ]);
    $client->submit($form) ;
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', '2023-12-01 2023-12-11 500');
    }

}
